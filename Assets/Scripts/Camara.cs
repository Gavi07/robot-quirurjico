using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{

    public float camx = 0.2f, camy = 0.5f;
    public Camera cam1, cam2;
    public float zoom = 30, minZoom = 2, maxZoom = 30;

    public Vector3 PosPlayer;

    private GUISkin MySkin;

    // Use this for initialization
    void Start()
    {
        cam1.enabled = true;
        cam2.enabled = true;
       // MySkin = Resources.Load<GUISkin>("MyGUISKIN");
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetButton("Boton7")) {
           
        transform.Translate(Input.GetAxis("EjeX") * Time.deltaTime * camy, Input.GetAxis("EjeY") * Time.deltaTime * camy, Input.GetAxis("EjeY2") * Time.deltaTime * camy);
    }
        if (Input.GetButton("Boton8"))
        {
            transform.Rotate(Input.GetAxis("EjeX") * Time.deltaTime * camx, Input.GetAxis("EjeY") * Time.deltaTime * camx, 0f);
        }*/

        PosPlayer = GameObject.FindGameObjectWithTag("PinzaLapbot").transform.position;

        if (Input.GetButton("Boton8"))
        {
            zoom += Input.GetAxis("EjeY") * camx;
            zoom = Mathf.Clamp(zoom, minZoom, maxZoom);
            cam2.fieldOfView = zoom;
            //transform.Translate(Input.GetAxis("EjeX") * Time.deltaTime * camy, 0f, 0f);
        }

        //Debug.Log(PosPlayer.x);


    }

    /*void OnGUI()
    {
        GUI.skin = MySkin;
        GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(Screen.width / 800.0f, Screen.height / 600.0f, 1));
        GUI.Label(new Rect(0, 0, 450, 200), "Posicion: " + PosPlayer);

        PlayerPrefs.SetFloat("x", PosPlayer.x);
        PlayerPrefs.SetFloat("y", PosPlayer.y);
        PlayerPrefs.SetFloat("z", PosPlayer.z);

        
    }*/
}

using UnityEngine;
using System.Collections;

public class MGI : MonoBehaviour
{

    public Vector3 PosPlayer;
    private double x, y, z; //x=0.415049, y = -0.00319701, z=-0.09656
    private double TrocarX = -0.024, TrocarY = 0.6891, TrocarZ = -0.2379;//TrocarX=0.4,TrocarY=0, TrocarZ=0.2
    private double A1, A2, B1, B2, s23, c23, K, M, N, D, B, E1, E2, t2a1, t2a2, t2a3, c2, s2, c3, s3, c4, s4, c5, s5, c6, s8, c8, s6, a1, a2, a3, a4, a5, a6, a7, a8, a9;
    private double a10, auxa, aur1, aur2, au3, au4, au5, au6, au7, au8, au9, au10, au11, D1, D2, R1, R2, R3, sx, sy, sz, nx, ny, nz, ax, ay, az, tx, ty, tz, r1, t2, t3, t4, t5, t6, t7, t8, t9;
    [HideInInspector]
    public float t2g, t3g, t4g, t5g, t6g, t7g, t8g, t9g, r1f;
    [HideInInspector]
    public float xf, yf, zf;


    //private Rigidbody rb;


    // Use this for initialization
    void Start()
    {

        // rb = gameObject.GetComponent<Rigidbody>();


    }
    // Update is called once per frame
    void Update()
    {
        // obtengo la posicion de la pinza 
        // Captura pos = GameObject.Find("art5_art6_L").GetComponent<Captura>();

        // x = Mathf.Abs(System.Convert.ToSingle(pos.posx / (-62.604041209)));//
        //Debug.Log(x);
        //  y = pos.posy / -438.399974;//
        //   z = pos.posz / -415.5627565;//
        PosPlayer = GameObject.FindGameObjectWithTag("PinzaLapbot").transform.position;

        x = PosPlayer.x;
        y = PosPlayer.y;
        z = PosPlayer.z;

        //Debug.Log(PosPlayer.x);

        t4 = System.Math.PI / 4;

        D1 = 0.25;
        D2 = 0.2;
        R1 = 0.2;
        R2 = 0.3;
        R3 = 0.01;
        sx = 1;
        sy = 0;
        sz = 0;
        nx = 0;
        ny = -1;
        nz = 0;
        ax = 0;
        ay = 0;
        az = -1;

        //trocar del labot
        tx = TrocarX;
        ty = TrocarY;
        tz = TrocarZ;

        K = -R1 * System.Math.Cos(t4) + az * R3 - z;
        M = (System.Math.Pow(az, 2) + System.Math.Pow(ay, 2) + System.Math.Pow(ax, 2)) * System.Math.Pow(R3, 2) + (2 * ty * ay - 2 * ay * y - 2 * ax * x - 2 * az * z + 2 * tx * ax + 2 * az * tz) * R3 - 2 * z * tz + System.Math.Pow(y, 2) - 2 * tx * x - 2 * y * ty + System.Math.Pow(z, 2) + System.Math.Pow(tx, 2) + System.Math.Pow(tz, 2) + System.Math.Pow(x, 2) + System.Math.Pow(ty, 2);
        N = System.Math.Pow(az * R3 - z + tz, 2);

        //La solución para r1 es:

        r1 = System.Math.Sqrt(System.Math.Pow(R2, 2) * N / M) - K;

        D = (-System.Math.Sin(t4) * R1 - D2) * (-az * R3 + z - tz);
        B = -D1 * (-az * R3 + z - tz);
        E1 = -ty * (-az * R3 + z - tz) + (tz + System.Math.Cos(t4) * R1 - r1) * (-ay * R3 + y - ty);
        E2 = (tz + System.Math.Cos(t4) * R1 - r1) * (-ax * R3 + x - tx) - tx * (-az * R3 + z - tz);

        //Las soluciones para t2 y t3 son:

        t2a1 = System.Math.Pow(E1, 2) + System.Math.Pow(E2, 2);
        t2a2 = (t2a1 + System.Math.Pow(B, 2) - System.Math.Pow(D, 2)) / (2 * B);
        t2a3 = System.Math.Sqrt(t2a1 - System.Math.Pow(t2a2, 2));
        c2 = (E2 * t2a2 - E1 * t2a3) / t2a1;
        s2 = (E1 * t2a2 + E2 * t2a3) / t2a1;
        t2 = System.Math.Atan2(s2, c2);//c2


        t3 = System.Math.Atan2(E1 - B * s2, E2 - B * c2) - t2;

        s23 = System.Math.Sin(t2 + t3);
        c23 = System.Math.Cos(t2 + t3);
        c3 = System.Math.Cos(t3);
        s3 = System.Math.Sin(t3);
        s4 = System.Math.Sin(t4);
        c4 = System.Math.Cos(t4);

        A2 = -r1 * s4 * ty + tz * s4 * ty + D1 * c4 * tx * s3 + System.Math.Pow(ty, 2) * c4 * c2 * s3 + System.Math.Pow(ty, 2) * c4 * s2 * c3 + System.Math.Pow(D2, 2) * c2 * s3 * c4 - D1 * c4 * tx * s3 * System.Math.Pow(c2, 2) - r1 * s2 * c3 * R1 * System.Math.Pow(c4, 2) + s2 * D1 * c4 * D2 - 2 * D2 * s2 * System.Math.Pow(c3, 2) * c4 * tx * c2 - 2 * c4 * R1 * s4 * System.Math.Pow(c2, 2) * c3 * tx * s3 - 2 * D2 * System.Math.Pow(c2, 2) * s3 * c4 * tx * c3 + D2 * c2 * s3 * c4 * D1 * c3 + 2 * D2 * System.Math.Pow(c2, 2) * c4 * ty * System.Math.Pow(c3, 2) - 2 * D2 * c2 * s3 * c4 * ty * s2 * c3 + D2 * c2 * s3 * c4 * s4 * R1 + s2 * D1 * c4 * s4 * R1 * System.Math.Pow(c3, 2) + D2 * c3 * c4 * tx * s3 + c4 * R1 * s4 * s2 * tx * c2 + D2 * c2 * c4 * tx * s2 + s2 * System.Math.Pow(D1, 2) * c4 * c3 + tz * s2 * c3 * R1 * System.Math.Pow(c4, 2) + tz * c2 * s3 * R1 * System.Math.Pow(c4, 2) + D2 * s2 * c3 * c4 * s4 * R1 + D2 * s2 * System.Math.Pow(c3, 2) * c4 * D1 + r1 * s4 * s2 * c3 * D2 + ty * c4 * D1 * c3 * System.Math.Pow(c2, 2) + c4 * R1 * s4 * s3 * tx * c3 - ty * c4 * D2 - 2 * ty * c4 * D1 * c3 - D2 * System.Math.Pow(c3, 2) * c4 * ty - tz * s4 * s2 * D1 - tz * c2 * s3 * R1 - tz * s2 * c3 * R1 - D2 * System.Math.Pow(c2, 2) * c4 * ty + System.Math.Pow(D2, 2) * s2 * c3 * c4 + c4 * R1 * s4 * c2 * c3 * D1 * s3 - s2 * D1 * c4 * ty * c2 * s3 - s2 * D1 * c4 * tx * c2 * c3 - 2 * c4 * R1 * s4 * c2 * System.Math.Pow(c3, 2) * tx * s2 + r1 * c2 * s3 * R1 + r1 * s4 * c2 * s3 * D2 + ty * c4 * tx * c2 * c3 + 2 * c4 * R1 * s4 * System.Math.Pow(c2, 2) * System.Math.Pow(c3, 2) * ty - ty * c4 * tx * s2 * s3 - ty * c4 * s4 * R1 * System.Math.Pow(c2, 2) - r1 * c2 * s3 * R1 * System.Math.Pow(c4, 2) - ty * c4 * s4 * R1 * System.Math.Pow(c3, 2) + r1 * s2 * c3 * R1 - tz * s4 * c2 * s3 * D2 - tz * s4 * s2 * c3 * D2 - 2 * c4 * R1 * s4 * c2 * c3 * ty * s2 * s3 + r1 * s4 * s2 * D1;
        B2 = s2 * D1 * tx * c2 * s3 - D2 * c2 * D1 - s2 * System.Math.Pow(D1, 2) * s3 + 2 * R1 * c2 * s3 * s4 * tx * s2 * c3 + 2 * D2 * s2 * c3 * tx * c2 * s3 - 2 * R1 * System.Math.Pow(c2, 2) * s4 * tx * System.Math.Pow(c3, 2) - 2 * R1 * System.Math.Pow(c2, 2) * s3 * s4 * ty * c3 - 2 * D2 * s2 * System.Math.Pow(c3, 2) * ty * c2 - 2 * D2 * System.Math.Pow(c3, 2) * tx * System.Math.Pow(c2, 2) - D1 * tx * c3 * System.Math.Pow(c2, 2) - ty * tx * c2 * s3 - 2 * R1 * s2 * System.Math.Pow(c3, 2) * s4 * ty * c2 - 2 * D2 * System.Math.Pow(c2, 2) * s3 * ty * c3 - ty * tx * s2 * c3 - R1 * c2 * s4 * D1 - ty * D1 * s3 * System.Math.Pow(c2, 2) + D2 * c3 * ty * s3 - System.Math.Pow(ty, 2) * s2 * s3 + D2 * System.Math.Pow(c3, 2) * tx + D2 * System.Math.Pow(c2, 2) * tx + 2 * ty * D1 * s3 - s2 * D1 * ty * c2 * c3 + R1 * System.Math.Pow(c3, 2) * s4 * tx + D2 * c2 * ty * s2 + D2 * c2 * D1 * System.Math.Pow(c3, 2) + R1 * System.Math.Pow(c2, 2) * s4 * tx + R1 * c2 * s4 * D1 * System.Math.Pow(c3, 2) + R1 * c2 * s4 * ty * s2 + R1 * c3 * s4 * ty * s3 - D2 * s2 * c3 * D1 * s3 - R1 * s2 * c3 * s4 * D1 * s3 + D1 * tx * c3 + System.Math.Pow(ty, 2) * c2 * c3;

        // Las solución para t5:
        t5 = System.Math.Atan2(-B2, A2);

        s5 = System.Math.Sin(t5);
        c5 = System.Math.Cos(t5);

        A1 = -D1 * c4 * c5 * s3 + D2 * s5 + s4 * R1 * s5 + tx * c4 * c5 * c2 * s3 + tx * s5 * s2 * s3 + D1 * s5 * c3 + tx * c4 * c5 * s2 * c3 - ty * c4 * c5 * c2 * c3 - ty * s5 * c2 * s3 + ty * c4 * c5 * s2 * s3 - ty * s5 * s2 * c3 - tx * s5 * c2 * c3;
        B1 = -D1 * s4 * s3 + tx * s4 * s2 * c3 + ty * s4 * s2 * s3 - ty * s4 * c2 * c3 + tx * s4 * c2 * s3;

        // Para t5:
        t6 = System.Math.Atan2(-B1, A1);
        s6 = System.Math.Sin(t6);
        c6 = System.Math.Cos(t6);

        a1 = s23 * c5;
        a2 = c23 * c5;
        a3 = s23 * s5;
        a4 = c23 * s5;
        a5 = s23 * c4;
        a6 = c23 * c4;
        a7 = s23 * s4;
        a8 = c23 * s4;
        a9 = s4 * s5;
        a10 = c4 * s6;

        auxa = s4 * c6;
        aur1 = s4 * c5 * s6;
        aur2 = c4 * c6;

        au3 = a8 * s6;
        au4 = s5 * s23 * c6;
        au5 = c6 * a6 * c5;
        au6 = a7 * s6;
        au7 = c6 * a4;
        au8 = c6 * a5 * c5;
        au9 = s4 * c5 * c6;
        au10 = s6 * a6 * c5;
        au11 = s6 * a5 * c5;

        //Para t7, t8, t9:
        t7 = System.Math.Atan2(((-a6 * s5 + a1) * ax + (-a5 * s5 - a2) * ay - a9 * az), ((-au3 + au4 + au5) * ax + (-au6 - au7 + au8) * ay + (au9 + a10) * az));

        s8 = System.Math.Sqrt((System.Math.Pow(((-au3 + au4 + au5) * ax + (-au6 - au7 + au8) * ay + (au9 + a10) * az), 2) + System.Math.Pow(((-a6 * s5 + a1) * ax + (-a5 * s5 - a2) * ay - a9 * az), 2)));
        c8 = (-(-au10 - s6 * a3 - c23 * auxa) * ax - (-au11 + s6 * a4 - s23 * auxa) * ay - (-aur1 + aur2) * az);
        t8 = System.Math.Atan2(s8, c8);

        t9 = System.Math.Atan2((-(-au10 - s6 * a3 - c23 * auxa) * nx - (-au11 + s6 * a4 - s23 * auxa) * ny - (-aur1 + aur2) * nz), ((-au10 - s6 * a3 - c23 * auxa) * sx + (-au11 + s6 * a4 - s23 * auxa) * sy + (-aur1 + aur2) * sz));


        //convertir radianes a grados 
        r1f = System.Convert.ToSingle(r1);// base
        //Debug.Log(r1f);
        t2g = System.Convert.ToSingle(t2) * Mathf.Rad2Deg;//art2...
        t3g = System.Convert.ToSingle(t3) * Mathf.Rad2Deg;
        t4g = System.Convert.ToSingle(t4) * Mathf.Rad2Deg;
        t5g = System.Convert.ToSingle(t5) * Mathf.Rad2Deg;
        t6g = System.Convert.ToSingle(t6) * Mathf.Rad2Deg;
        t7g = System.Convert.ToSingle(t7) * Mathf.Rad2Deg;
        t8g = System.Convert.ToSingle(t8) * Mathf.Rad2Deg;
        t9g = System.Convert.ToSingle(t9) * Mathf.Rad2Deg;

        //Debug.Log(t2g);

        //convertir double a float

        xf = System.Convert.ToSingle(x);
        yf = System.Convert.ToSingle(y);
        zf = System.Convert.ToSingle(z);

        
    }

    void OnGUI()
    {


        string Texto = GUI.TextField(new Rect(10.0f, 30.0f, 50.0f, 20.0f), r1.ToString());
        string Texto2 = GUI.TextField(new Rect(10.0f, 50.0f, 50.0f, 20.0f), t2g.ToString());
        string Texto3 = GUI.TextField(new Rect(10.0f, 70.0f, 50.0f, 20.0f), t3g.ToString());
        string Texto4 = GUI.TextField(new Rect(10.0f, 90.0f, 50.0f, 20.0f), t4g.ToString());
        string Texto5 = GUI.TextField(new Rect(10.0f, 110.0f, 50.0f, 20.0f), t5g.ToString());
        string Texto6 = GUI.TextField(new Rect(10.0f, 130.0f, 50.0f, 20.0f), t6g.ToString());
        string Texto7 = GUI.TextField(new Rect(10.0f, 150.0f, 50.0f, 20.0f), t7g.ToString());
        string Texto8 = GUI.TextField(new Rect(10.0f, 170.0f, 50.0f, 20.0f), t8g.ToString());
        string Texto9 = GUI.TextField(new Rect(10.0f, 190.0f, 50.0f, 20.0f), t9g.ToString());



    }
}

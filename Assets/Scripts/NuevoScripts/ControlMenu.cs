﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMenu : MonoBehaviour {

    public GameObject[] botones;
    private float posicionInicial;
    private bool estado;
    public GameObject[] camaras;
    public GameObject[] robots;

	// Use this for initialization
	void Start () {
        posicionInicial = botones[1].transform.position.x;
        estado = true;

        camaras[0].GetComponent<Camera>().enabled = true;
        camaras[1].GetComponent<Camera>().enabled = false;

        robots[0].GetComponent<ControlRobotAmarillo>().enabled = true;
        robots[1].GetComponent<ControlRobotAzul>().enabled = false;
        robots[2].GetComponent<ControlRobotInterno>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Minimizar()
    {
        Debug.Log("Entro a MENU");
        if (estado)
        {
            for (int i = 1; i < botones.Length-1; i++)
            {
                botones[i].transform.position = new Vector3(botones[0].transform.position.x, botones[i].transform.position.y, botones[i].transform.position.z);
                botones[botones.Length - 1].transform.eulerAngles = new Vector3(0, 0, 180);
            }
            estado = false;
        }
        else
        {
            for (int i = 1; i < botones.Length-1; i++)
            {
                botones[i].transform.position = new Vector3(posicionInicial, botones[i].transform.position.y, botones[i].transform.position.z);
                botones[botones.Length - 1].transform.eulerAngles = new Vector3(0, 0, 0);
            }
        }
    }

    public void activarCamaraExterior()
    {
        camaras[0].GetComponent<Camera>().enabled = true;
        camaras[1].GetComponent<Camera>().enabled = false;
    }

    public void activarCamaraInterior()
    {
        camaras[0].GetComponent<Camera>().enabled = false;
        camaras[1].GetComponent<Camera>().enabled = true;
    }

    public void robotAmarillo()
    {
        robots[0].GetComponent<ControlRobotAmarillo>().enabled = true;
        robots[1].GetComponent<ControlRobotAzul>().enabled = false;
        robots[2].GetComponent<ControlRobotInterno>().enabled = false;
    }

    public void robotAzul()
    {
        robots[0].GetComponent<ControlRobotAmarillo>().enabled = false;
        robots[1].GetComponent<ControlRobotAzul>().enabled = true;
        robots[2].GetComponent<ControlRobotInterno>().enabled = false;
    }

    public void robotInterno()
    {
        robots[0].GetComponent<ControlRobotAmarillo>().enabled = false;
        robots[1].GetComponent<ControlRobotAzul>().enabled = false;
        robots[2].GetComponent<ControlRobotInterno>().enabled = true;
    }

}

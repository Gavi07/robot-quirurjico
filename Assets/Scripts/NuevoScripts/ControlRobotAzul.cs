﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRobotAzul : MonoBehaviour {

    public float velocidad = 1;
    public GameObject[] brazo;
    private bool controlActivo;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow) && controlActivo)
            brazo[0].transform.Rotate(Vector3.back * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.DownArrow) && controlActivo)
            brazo[0].transform.Rotate(-Vector3.back * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow) && controlActivo)
            brazo[2].transform.Rotate(Vector3.up * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow) && controlActivo)
            brazo[2].transform.Rotate(-Vector3.up * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.A) && controlActivo)
            brazo[1].transform.Rotate(Vector3.forward * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.Z) && controlActivo)
            brazo[1].transform.Rotate(-Vector3.forward * velocidad * Time.deltaTime);

        if ((Input.GetKey(KeyCode.UpArrow) ||
            Input.GetKey(KeyCode.DownArrow) ||
            Input.GetKey(KeyCode.RightArrow) ||
            Input.GetKey(KeyCode.LeftArrow) ||
            Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.Z)) && !controlActivo)
            desactivarControl();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Lo toco");
        desactivarControl();
    }

    /*void OnTriggerExit(Collider other)
    {
        Debug.Log("Lo toco");
        activarControl();
    }*/

    public void activarControl()
    {
        Debug.Log("Lo activo");
        controlActivo = true;
    }

    public void desactivarControl()
    {
        brazo[0].transform.Rotate(Vector3.back * 0.1f);
        controlActivo = false;
        Invoke("activarControl", 2);
    }
}

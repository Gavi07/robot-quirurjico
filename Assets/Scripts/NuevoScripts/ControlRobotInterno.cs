﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRobotInterno : MonoBehaviour {

    public float velocidad = 1;
    public float velocidadRotacion = 40;
    private bool controlActivo;

    // Use this for initialization
    void Start () {
        controlActivo = true;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.UpArrow) && controlActivo)
            transform.Translate(Vector3.forward * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.DownArrow) && controlActivo)
            transform.Translate(-Vector3.forward * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow) && controlActivo)
            transform.Translate(Vector3.right * velocidad * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow) && controlActivo)
            transform.Translate(-Vector3.right * velocidad * Time.deltaTime);

        transform.Rotate((Input.GetAxis("Mouse Y") * velocidadRotacion * Time.deltaTime),
                         (Input.GetAxis("Mouse X") * velocidadRotacion * Time.deltaTime),
                         0);

    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Lo toco");
        desactivarControl();
    }

    public void activarControl()
    {
        Debug.Log("Lo activo");
        controlActivo = true;
    }

    public void desactivarControl()
    {
        transform.Translate(-Vector3.forward * 0.001f);
        controlActivo = false;
        Invoke("activarControl", 2);
    }
}

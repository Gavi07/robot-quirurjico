public function BotonHombre()
{
	SceneManagement.SceneManager.LoadScene("Hombre");
}

public function BotonNino()
{
	SceneManagement.SceneManager.LoadScene("Nino");
}

public function BotonMujer()
{
	SceneManagement.SceneManager.LoadScene("Mujer");
}

public function BotonSalir()
{
	Application.Quit();
}

public function BotonVolver()
{
	SceneManagement.SceneManager.LoadScene("Menu");
}